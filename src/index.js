jQuery(function($) {
  const $body = jQuery( 'body' ),
        $navigation = $body.find( '.navigation-top' ),
        $menuToggle = $navigation.find( '.menu-toggle' ),
        $customHeader = $body.find( '.custom-header' ),
        $branding = $customHeader.find( '.site-branding' ),
        navigationOuterHeight = $navigation.outerHeight(),
        navigationFixedClass = 'site-navigation-fixed',
        isFrontPage = $body.hasClass( 'twentyseventeen-front-page' ) || $body.hasClass( 'home blog' );

  adjustChildHeaderHeight();

  // Set margins of branding in header.
  function adjustChildHeaderHeight() {
    if ( 'none' === $menuToggle.css( 'display' ) ) {

      // The margin should be applied to different elements on front-page or home vs interior pages.
      if ( isFrontPage ) {
        $branding.css( 'margin-top', navigationOuterHeight );
      } else {
        $customHeader.css( 'margin-top', navigationOuterHeight );
      }
      $customHeader.css( 'margin-bottom', '0' );
      $branding.css( 'margin-bottom', '0' );
    }

  }

  // Ensure the sticky navigation doesn't cover current focused links.
	$( 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex], [contenteditable]', '.site-content-contain' ).filter( ':visible' ).focus( function() {
		if ( $navigation.hasClass( 'site-navigation-fixed' ) ) {
			var windowScrollTop = $( window ).scrollTop(),
				fixedNavHeight = $navigation.height(),
				itemScrollTop = $( this ).offset().top,
				offsetDiff = itemScrollTop - windowScrollTop;

			// Account for Admin bar.
			if ( $( '#wpadminbar' ).length ) {
				offsetDiff -= $( '#wpadminbar' ).height();
			}

			if ( offsetDiff < fixedNavHeight ) {
				$( window ).scrollTo( itemScrollTop - ( fixedNavHeight + 50 ), 0 );
			}
		}
	});
});
