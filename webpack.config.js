const defaultConfig = require( '@wordpress/scripts/config/webpack.config' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );

module.exports = {
	...defaultConfig,
	module: {
		...defaultConfig.module,
		rules: [
			...defaultConfig.module.rules,
			{
				test: /\.css$/i,
				use: [
					{ loader: MiniCssExtractPlugin.loader },
					{ loader: 'css-loader' },
				],
			},
			{
				test: /\.(mp4|ogg|svg|eot|ttf|woff|woff2|jpg|png)$/,
				loader: 'file-loader',
			},
		],
	},
	plugins: [
		new MiniCssExtractPlugin( {
			filename: '[name].css',
			chunkFilename: '[id].css',
			ignoreOrder: false,
		} ),
	],
};
